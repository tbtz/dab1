1. Python 3 installieren
2. virtualenv installieren --> pip3 install virtualenv
3. virtuales environment erzeugen --> virtualenv env
4. virtualenv starten --> source env/bin/activate
5. Abhängigkeiten installieren --> pip3 install -r requirements.txt
6. Jupyter notebook starten --> jupyter notebook
