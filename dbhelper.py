import csv
import cx_Oracle
DB = None

def connect(username, password):
    global DB
    DB = cx_Oracle.connect(username+'/'+password+'@localhost/oracle')
    print('Connected to Oracle DB (Version: ' + DB.version + ').')

def close():
    global DB
    DB.close()         

def get(query):
    global DB
    cursor = DB.cursor()
    cursor.execute(query)
    results = []
    for result in cursor:
        results.append(list(result))
    cursor.close()
    return results

def getFromCsv(path):
    with open(path) as file:
        reader = csv.reader(file)
        next(reader) # Skip head
        results = []
        for result in reader:
            results.append(result)
        return results